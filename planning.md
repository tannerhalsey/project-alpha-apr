Django project named tracker 
    apps named
        accounts, 
        projects, 
        tasks
    
    add to project (tracker) settings installed apps

    migrate

    create superuser

    ------- FEATURE 2 IS WORKING AND COMMITTED---------

    Feature 3--

    CREATE PROJECT MODEL IN PROJECTS APP

import user
    class Project(models.Model):
        name = models.CharField(max_length=200)
        description = models.TextField()
        members = models.ManyToManyField(User, related_name='projects')

        def __str__(self):
            return self.name
    
    --------FEATURE 3 COMPLETE--------

    register model "Project" in admins.py

from .models import Project

admin.site.register(Project)

------FEAUTURE 4 COMPLETE-----

-create a list view for the Project model
-registering the view for a path
-registering the projects paths with the tracker project
-creating a template for the view.

-------create a list view for the Project model

class ProjectListView(ListView):
    model = Project
    template_name = "projects_list.html"
    context_object_name = "projects"
    
-------register view in urls.py for app-----

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
]

------register url patters in urls.py for project--------

urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
]
----------Feature 5---------------------------------------

create list view

projectlistview
"" list_projects

"projects/" include(projects.urls)

template will need dnajo if statement

class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)

add  redirectview
-------------FEATURE 7-9---------------


login view----accounts urls.py with the path "login/"

URL patterns from the accounts app in the tracker project
    -"accounts/"

-tempmplates directory under accounts
    - registration directory under templates
        -HTML template named login.html in the registration directory


tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home"

TEMPLATE SPECS---------
a main tag that contains
div tag that contains
an h1 tag with the content "Login"
a form tag with method "post" that contains any kind of HTML structures but must include
an input tag with type "text" and name "username"
an input tag with type "password" and name "password"
a button with the content "Login"




Import the LogoutView from the same module that you
 imported the LoginView from
 Register that view in your urlpatterns list with the path "logout/" and the name "logout"


 --------FEATURE 10----------

 -import the UserCreationForm from the built-in auth forms 
 -use the special create_user  method to
create a new user account from their username and password
- use the login  function that logs an account in
- redirect the browser to the path registered with the name "home"
-Create an HTML template named signup.html in the registration directory
-Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
--------Template specifications

the fundamental five in it
a main tag that contains
div tag that contains
an h1 tag with the content "Signup"
a form tag with method "post" that contains any kind of HTML structures but must include
an input tag with type "text" and name "username"
an input tag with type "password" and name "password1"
an input tag with type "password" and name "password2"
a button with the content "Signup"\



-------FEATURE 11-----------

class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User,
        related_name="tasks",
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self):
        return self.name



-----FEATURE 12--------
Register the Task model with the admin so that you can see it in the Django admin site.


--------FEATURE 13------

Create a view that shows the details of a particular project
A user must be logged in to see the view
In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
Create a template to show the details of the project and a table of its tasks
Update the list template to show the number of tasks for a project
Update the list template to have a link from the project name to the detail view for that project

Template specifications

the fundamental five in it
a main tag that contains
div tag that contains
an h1 tag with the project's name as its content
a p tag with the project's description in it
an h2 tag that has the content "Tasks"
if the project has tasks, then
a table that contains five columns with the headers "Name", "Assignee", "Start date", "Due date", and "Is completed" with rows for each task in the project
otherwise
a p tag with the content "This project has no tasks"

view----

class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"

----------FEATURE 14--------

Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
A person must be logged in to see the view
If the project is successfully created, it should redirect to the detail page for that project
Register that view for the path "create/" in the projects urls.py and the name "create_project"
Create an HTML template that shows the form to create a new Project (see the template specifications below)
Add a link to the list view for the Project that navigates to the new create view

class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "members"]
    template_name = "projects/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.save()
        return redirect("show_project", pk=item.id)


-------FEATURE 15--------
Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
The view must only be accessible by people that are logged in
When the view successfully handles the form submission, have it redirect to the detail page of the task's project
Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
Create a template for the create view that complies with the following specifications
Add a link to create a task from the project detail page that complies with the following specifications


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
    context_object_name = "update_task_list"


Template specifications-----

the fundamental five in it
a main tag that contains
div tag that contains
an h1 tag with the content "Create a new task"
a form tag with method "post" that contains any kind of HTML structures but must include
an input tag with type "text" and name "name"
an input tag with type "text" and name "start_date"
an input tag with type "text" and name "due_date"
a select tag with name "projects"
a select tag with name "assignee"

---------FEATURE 16----------
Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
The view must only be accessible by people that are logged in
Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
Create an HTML template that conforms with the following specification


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


------FEATUER 17------

Create an update view for the Task model that only is concerned with the is_completed field
When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
You do not need to make a template for this view
Modify the "My Tasks" view to comply with the template specification


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
    context_object_name = "update_task_list"



Template specification----
--In the table cell that holds the status for each task in the "My Tasks" view, if the value of is_completed is False, then show this form.


ADDD

<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>


FEATUER 18----
-nstall the django-markdownify package using pip
-put it into the INSTALLED_APPS in the tracker settings.py
-n the tracker settings.py file, add the configuration setting to disable sanitation 
-In your the template for the Project detail view, load the markdownify template library as shown in the Usage  section
-Replace the p tag and {{ project.description }} in the Project detail view with this code
-Use pip freeze to update your requirements.txt file


-----FEATURE 19--------


Template specification
On all HTML pages, add

a header tag as the first child of the body tag before the main tag that contains
a nav tag that contains
a ul tag that contains
if the person is logged in,
an li tag that contains
an a tag with an href to the "show_my_tasks" path with the content "My tasks"
an li tag that contains
an a tag with an href to the "list_projects" path with the content "My projects"
an li tag that contains
an a tag with an href to the "logout" path with the content "Logout"
otherwise
an li tag that contains
an a tag with an href to the "login" path with the content "Login"
an li tag that contains
an a tag with an href to the "signup" path with the content "Signup"


